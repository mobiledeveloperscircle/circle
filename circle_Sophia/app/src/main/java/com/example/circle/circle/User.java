package com.example.circle.circle;

/**
 * Created by sophia on 10/24/15.
 */

import java.util.ArrayList;

public class User {
    public String name;
    public String rank;
    public String frequent;

    public User(String name, String rank, String frequent) {
        this.name = name;
        this.rank = rank;
        this.frequent = frequent;
    }

    public static ArrayList<User> getUsers() {
        ArrayList<User> users = new ArrayList<User>();
        //Just list in alphabetical order
        users.add(new User("Aditya", "A", "Y"));
        users.add(new User("Aneesh", "A", "N"));
        users.add(new User("Genji", "B", "Y"));
        users.add(new User("Jimmy", "C","M"));
        return users;
    }
}