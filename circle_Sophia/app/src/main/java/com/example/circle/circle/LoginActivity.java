package com.example.circle.circle;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

/**
 * Created by sophia on 10/24/15.
 */
public class LoginActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);



        final Button button = (Button) findViewById(R.id.button1);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                ParseUser user = new ParseUser();
                EditText etemail = (EditText)findViewById(R.id.email);
                EditText etusername = (EditText)findViewById(R.id.username);
                EditText etpassword = (EditText)findViewById(R.id.password);
                String semail = etemail.getText().toString();
                String susername = etusername.getText().toString();
                String spassword = etpassword.getText().toString();

                user.setEmail(semail);
                user.setUsername(susername);
                user.setPassword(spassword);
                // other fields can be set just like with ParseObject
                user.put("phone", "510-000-0000");
//                user.put("u", susername);
//                user.put("pw", spassword);
                user.signUpInBackground(new SignUpCallback() {
                    public void done(ParseException e) {
                        if (e == null) {
                            // Hooray! Let them use the app now.
                        } else {
                            // Sign up didn't succeed. Look at the ParseException
                            // to figure out what went wrong
                        }
                    }
                });
            }
        });
    }
}
