package com.example.circle.circle;

import android.app.Activity;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by sophia on 10/24/15.
 */

public class UserArrayAdapter extends ArrayAdapter<User> {
    private final Activity context;
    private final ArrayList<User> users;

    public UserArrayAdapter(Activity context, ArrayList<User> users) {
        super(context, 0, users);
        this.context = context;
        this.users = users;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        User user = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_user, parent, false);
        }
        // Lookup view for data population
        TextView tvName = (TextView) convertView.findViewById(R.id.tvName);
        TextView tvHome = (TextView) convertView.findViewById(R.id.tvHometown);
        TextView tvFrequent = (TextView) convertView.findViewById(R.id.tvFrequent);
        //set background color based on friend level
        if(user.rank == "A"){
            convertView.setBackgroundColor(Color.RED);
        }else{
            convertView.setBackgroundColor(Color.WHITE);
        }
        // Populate the data into the template view using the data object
        tvName.setText(user.name);
        tvHome.setText(user.rank);
        tvFrequent.setText(user.frequent);

        // Return the completed view to render on screen
        return convertView;
    }
}
