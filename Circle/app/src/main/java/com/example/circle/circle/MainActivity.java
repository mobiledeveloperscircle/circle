package com.example.circle.circle;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

public class MainActivity extends Activity {
    //9:15pm

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //enable data storage
        Parse.enableLocalDatastore(this);
        Parse.initialize(this, "4QbDWMEYfZi9kQVqPA6VzXTM8HnLE4cWoqEhpA4y", "xTFxgHY16inl5vkafF3P5HSu98AOrZwTEuVVSPH7");

//        final ParseUser user = new ParseUser();
//        user.setEmail("test849@berkeley.edu");
//        user.setUsername("test849");
//        user.setPassword("test849");
//        // other fields can be set just like with ParseObject
//        user.put("phone", "849");
//        user.signUpInBackground(new SignUpCallback() {
//            public void done(ParseException e) {
//                if (e == null) {
//                    // Hooray! Let them use the app now.
//                } else {
//                    // Sign up didn't succeed. Look at the ParseException
//                    // to figure out what went wrong
//                }
//            }
//        });


//
        ParseObject testObject = new ParseObject("TestObject");
        testObject.put("foo2", "bar2");
        testObject.saveInBackground();

        super.onCreate(savedInstanceState);
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
