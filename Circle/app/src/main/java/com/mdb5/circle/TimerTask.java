package com.mdb5.circle;
import java.util.Timer;
import java.util.TimerTask;
import java.util.HashSet;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.CalendarContract;
import android.os.AsyncTask;
import android.text.format.DateUtils;
import java.util.Date;

/**
 * Created by JimmyCheung on 10/28/15.
 */
class Tasks extends AsyncTask<Void, Boolean, Boolean> {
    Timer timer = new Timer ();
    private final Context context;
    Tasks(Context cxt) {
        context = cxt;
    }
    TimerTask dailyTask = new TimerTask() {
        public void run() {
              final String[] FIELDS = new String[]{
                    CalendarContract.Calendars._ID,

            };
            Cursor cursor = null;
            ContentResolver cr = context.getContentResolver();
            cursor = cr.query(Uri.parse("content://com.android.calendar/events"), new String[]{ "calendar_id", "title", "description", "dtstart", "dtend", "eventLocation" }, null, null, null);
            HashSet<String[]> result = new HashSet<String[]>();
            HashSet<String> calendarIDs = new HashSet<String>();
            try {
                if (cursor.getCount() > 0) {
                    while (cursor.moveToNext()) {
                        String id = cursor.getString(0); // Date currently in unreadable format, need to change.
                        calendarIDs.add(id);
                    }
                }
            }
            catch (Exception e) {
                System.out.println(e);
            }
            for (String id : calendarIDs) {
                Uri.Builder builder = Uri.parse("content://com.android.calendar/instances/when").buildUpon();
                //Uri.Builder builder = Uri.parse("content://com.android.calendar/calendars").buildUpon();
                long now = new Date().getTime();

                ContentUris.appendId(builder, now);
                ContentUris.appendId(builder, now + DateUtils.DAY_IN_MILLIS * 7); // checks for next week

                Cursor eventCursor = cr.query(builder.build(),
                        new String[]{"begin", "end", "allDay"}, "Calendars._id=" + id,
                        null, "startDay ASC, startMinute ASC");
                if (eventCursor.getCount() > 0) {
                    eventCursor.moveToFirst();
                    do {

                        final Date begin = new Date(eventCursor.getLong(0));
                        final Date end = new Date(eventCursor.getLong(1));
                        final Boolean allDay = !eventCursor.getString(2).equals("0");

                        //not sure how to update data to parse - need to do here
                    } while (eventCursor.moveToNext());
                }
            }
        }
    };
    protected void doInBackground(Void... params) {
        timer.schedule(dailyTask, 0, 1000 * 60 * 60 * 24);
    }

}
